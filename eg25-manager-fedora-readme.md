## FAQ

### pinephone eg25-manager[467]: Unable to configure /dev/ttyS3

For eg25-manager to work on PPP one must:

1. Compile a kernel without CONFIG_MODEM_POWER
2. Apply this patch on top of megi's kernel: [1] (PPP), [2] (PP) OR use pine64 linux kernel like manjaro does

Unfortunately this will disable /sys/class/modem-power/modem-power/device/powered which is used by pp-start.sh
provided by pinephone-helpers-0.3.0-3.fc36.aarch64 so then to power on modem you MUST use eg25-manager.

[1] https://gitlab.manjaro.org/manjaro-arm/packages/core/linux-pinephonepro/-/blob/7b7254fefc3ea7babe7bc0e0232ebff31f80463d/rk3399-pinephone-pro-remove-powering-up-the-modem.patch
[2] https://gitlab.manjaro.org/manjaro-arm/packages/core/linux-pinephone/-/blob/5.15-megi/dts-pinephone-drop-modem-power-node.patch

### Rescheduling upload since modem isn't online yet, in 30s (happening forever)

See above: you have disabled CONFIG_MODEM_POWER, but You haven't applied a patch.

### -bash: /sys/class/modem-power/modem-power/device/powered: No such file or directory

Comment out everything related to modem initialization from /usr/bin/pp-start.sh
