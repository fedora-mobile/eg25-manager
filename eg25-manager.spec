Name:       eg25-manager
Version:    0.4.3
Release:    2%{?dist}
Summary:    A daemon for managing the Quectel EG25 modem found on the Pine64 PinePhone

License:    GPLv3+
URL:        https://gitlab.com/mobian1/devices/eg25-manager/
Source0:    https://gitlab.com/mobian1/devices/eg25-manager/-/archive/%{version}/%{name}-%{version}.tar.gz
Source1:    eg25-manager-fedora-readme.md

Patch1:     0001-suspend-increase-timeout-to-get-rid-of-resets.patch
Patch2:     0002-data-auto-select-a-VoLTE-profile.patch

BuildRequires:  gcc
BuildRequires:  gcc-c++
BuildRequires:  meson
BuildRequires:  cmake
BuildRequires:  scdoc

BuildRequires:  pkgconfig(glib-2.0)
BuildRequires:  pkgconfig(gio-unix-2.0)
BuildRequires:  pkgconfig(gudev-1.0)
BuildRequires:  pkgconfig(libgpiod)
BuildRequires:  pkgconfig(libusb-1.0)
BuildRequires:  pkgconfig(mm-glib)
BuildRequires:  pkgconfig(libcurl)


%description
eg25-manager is a daemon for managing the Quectel EG25 modem found on
the Pine64 PinePhone.

It implements the following features:

* cleanly power on/off the modem
* configure/check essential parameters (such as the audio format) on startup
* monitor the modem state through ModemManager
* put the modem in low-power mode when suspending the system, and restore it
* back to normal behavior when resuming
* monitor the modem state on resume and recover it if needed
* AGPS support
* VoLTE support

%prep
%setup -q -n %{name}-%{version}
%patch1 -p1
%patch2 -p1

%build
cp %{SOURCE1} README.Fedora.md
%meson
%meson_build

%install
%meson_install

%post
%systemd_post eg25-manager.service
systemctl enable eg25-manager.service

%preun
%systemd_preun eg25-manager.service

%postun
%systemd_postun_with_restart eg25-manager.service


%files
%{_bindir}/eg25-manager
%{_prefix}/lib/systemd/system/eg25-manager.service
%{_prefix}/lib/udev/rules.d/80-modem-eg25.rules
%{_datadir}/eg25-manager
%{_mandir}/man5/eg25-manager.5*
%{_mandir}/man8/eg25-manager.8*
%doc README.md README.Fedora.md
%license COPYING

%changelog
* Sat Mar 5 2022 marcin - 0.4.3-2
- increase wake from suspend timeout from 9 to 13s
- auto select a VoLTE profile

* Fri Mar 4 2022 marcin - 0.4.3-1
- update to 0.4.3
- clean up specfile a little
- add README describing how to make it work

* Mon Apr 12 2021 Tor - 0.3.0-1
- update to 0.3.0

* Thu Dec 17 2020 Tor - 0.2.1-1
- Initial packaging
